---
layout: post
title:  "Delete git's initial commit!"
date:   2019-03-06
categories: git
---

So, the other day, I forked/cloned this Jekyll project template which had a bunch of commits already in its commit history.

After grabbing the commit hash of the root-commit and a quick `git reset put_hash_here`, I removed the commit log, except one... The "Initial commit" 

I am almost finished, I said to myself. Or so I thought... It took me nearly an hour to find the solution I was looking for. 

<hr>
**It is important** at this point to keep your HEAD's hash in order to bring things back to the way they are.

Mine is `1c70a7425cd616a5a65c7d8ad0646324672d6233` as it is shown by the output of `git log`.

```bash
$ (master) git log
commit 1c70a7425cd616a5a65c7d8ad0646324672d6233 (HEAD -> master, origin/master)
Author: Mr Configured <mr.configured@gmail.com>
Date:   Tue Mar 5 22:13:00 2019 +0000

    Update _config.yml
```

**If things go wrong along the process, just type:**
```
$ echo '<top hash from git log>' > .git/refs/heads/<branch you are in>
```
In my case, ```$ echo '1c70a7425cd616a5a65c7d8ad0646324672d6233' > .git/refs/heads/master```
<hr>

Now, I am at the point where this initial commit is tormenting me and won't go away. So I type this:

```
git update-ref -d HEAD
```

This command is deleting the reference between the HEAD and the last commit's hash, by deleting the reference file `.git/refs/heads/<current_branch>`. This way you end up on the same branch, as the reference in the `.git/HEAD` file, but the HEAD is pointing to a non-existing file, hence **no history**.

By commiting the changes to the branch you are on, you have a brand new commit log.